This is minesweeper game (Saper). It's written in C and uses SDL2.

To compile and launch following packages have to be installed:

Fedora:

sudo dnf install SDL2 SDL2-devel SDL2_image SDL2_image-devel SDL2_ttf\
SDL2_ttf-devel

Ubuntu 23.10*:

sudo apt install libsdl2-2.0-0 libsdl2-dev libsdl2-image-2.0-0\
libsdl2-image-dev libsdl2-ttf-2.0-0 libsdl2-ttf-dev

* Ubuntu 22.04 LTS is not supported due to bug in some library or compositor.
Personally, I don't recommend Ubuntu, because it lost its quality after becoming
Microsoft's vassal. It's so buggy it even disabled 3D acceleration for guest in
Gnome boxes and Virt-manager. Furthermore, it's using Snaps that make its core
applications slughish and has suboptimal kernel config aimed neither for
desktops nor for servers.

Fedora, Debian, Arch Linux are way to go.
