#include "common.h"

extern int sdl_init(void);
extern void disp_init(SDL_DisplayMode *screen_res, SDL_Window **window,
                      SDL_Renderer **renderer);
extern int welcome(SDL_DisplayMode *screen_res, SDL_Renderer *renderer);
extern int menu(SDL_DisplayMode *screen_res, SDL_Renderer *renderer);
extern void settings(SDL_DisplayMode *screen_res, SDL_Renderer *renderer,
                     struct sets *sets);
extern int brd_set(struct brd **pbrd[], struct sets *sets);
extern void brd_free(struct brd **pbrd[], int size);
extern int game(SDL_DisplayMode *screen_res, SDL_Renderer *renderer,
                struct brd *pbrd[], struct sets *sets, int *empty_fields,
                bool *first_click, long int *time_end);
extern int end_dial(SDL_DisplayMode *screen_res, SDL_Renderer *renderer,
                    int empty_fields, long int time_end);

int main(void)
{
        if (sdl_init() < 0)
                return -1;

        SDL_Window *window = NULL;
        SDL_Renderer *renderer = NULL;
        SDL_DisplayMode screen_res;

        disp_init(&screen_res, &window, &renderer);

        struct sets sets = {0, 0};
        struct brd **pbrd = NULL;
        int empty_fields;
        bool first_click;
        long int time_end;

        int option = welcome(&screen_res, renderer);

        while (option != QUIT) {
                switch (option) {
                case MENU:
                        option = menu(&screen_res, renderer);
                        break;
                case SETTINGS:
                        settings(&screen_res, renderer, &sets);
                        empty_fields = brd_set(&pbrd, &sets);
                        option = GAME;
                        break;
                case GAME:
                        first_click = true;
                        option = game(&screen_res, renderer, pbrd, &sets, 
                                      &empty_fields, &first_click, &time_end);
                        brd_free(&pbrd, sets.size);
                        break;
                case END_GAME:
                        option = end_dial(&screen_res, renderer, empty_fields,
                                          time_end);
                        break;
                case QUIT:
                        break;
                }
        }
        
        return 0;
}
