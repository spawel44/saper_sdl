#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "common.h"

static void brd_alloc(struct brd **pbrd[], struct sets *sets)
{
        switch (sets->size) {
        case 0:
                sets->size = 10;
                break;
        case 1:
                sets->size = 20;
                break;
        case 2:
                sets->size = 30;
                break;
        }

        struct brd **tbrd;

        if ((tbrd = calloc((size_t) sets->size, sizeof(struct brd *))) == NULL) {
                puts("Not enough memory");
                exit(EXIT_FAILURE);
        } else {
                for (int i = 0; i < sets->size; ++i)
                        if ((tbrd[i] = calloc((size_t) sets->size,
                            sizeof(struct brd))) == NULL) {
                                puts("Not enough memory");
                                exit(EXIT_FAILURE);
                        }
        }

        *pbrd = tbrd;
}

static inline int valid_scope(int val, int range)
{
        if (val >= 0 && val < range)
                return 1;
        else
                return 0;
}

static void mine_dec_arnd(struct brd *pbrd[], int row, int col, int n, int m)
{
        if (pbrd[row + n][col + m].content < MINE &&
            pbrd[row + n][col + m].content > EMPTY)
                --pbrd[row + n][col + m].content;
}

static void mine_inc_here(struct brd *pbrd[], int row, int col, int n, int m)
{
        if (pbrd[row + n][col + m].content == MINE)
                ++pbrd[row][col].content;
}

static void mine_inc_arnd(struct brd *pbrd[], int row, int col, int n, int m)
{
        if (pbrd[row + n][col + m].content != MINE)
                ++pbrd[row + n][col + m].content;
}

static void brd_loop(struct brd *pbrd[], int row, int col, int size,
                     void (*fptr)(struct brd **, int, int, int, int))
{
        for (int n = -1; n < 2; ++n)
                for (int m = -1; m < 2; ++m)
                        if ((valid_scope(row + n, size) &&
                             valid_scope(col + m, size))) {
                                fptr(pbrd, row, col, n, m);
                        }
}

static int brd_fill(struct brd *pbrd[], struct sets *sets)
{
        int mines = 0;
        switch (sets->difclty) {
                case 0:
                        mines = sets->size * sets->size * 15 / 100;
                        break;
                case 1:
                        mines = sets->size * sets->size * 25 / 100;
                        break;
                case 2:
                        mines = sets->size * sets->size * 35 / 100;
                        break;
        }

        time_t t;
        srand((unsigned) time(&t));

        int mine_row, mine_col;
        for (int i = 0; i < mines; ++i) {
                mine_row = rand() % (sets->size - 1);
                mine_col = rand() % (sets->size - 1);

                if (pbrd[mine_row][mine_col].content == MINE) {
                        --i;
                        continue;
                } else {
                        pbrd[mine_row][mine_col].content = MINE;
                }
        }

        for (int row = 0; row < sets->size; ++row)
                for (int col = 0; col < sets->size; ++col)
                        if (pbrd[row][col].content == MINE)
                                brd_loop(pbrd, row, col, sets->size,
                                         &mine_inc_arnd);

        return mines;
}

static int empty_reveal(struct brd *pbrd[], int size, int row, int col)
{
        struct pos s[10000];
        bool stack = true;
        s[0] = (struct pos){row, col};
        int revealed = 0;

        for (int i = 0, j = 0; i >= 0; --i) {
                if (!pbrd[s[i].row][s[i].col].revealed) {
                        pbrd[s[i].row][s[i].col].revealed = true;
                        ++revealed;
                }

                if (stack) {
                        j = i;
                }

                if (pbrd[s[i].row][s[i].col].content == EMPTY) {
                        for (int n = -1; n < 2; ++n)
                        for (int m = -1; m < 2; ++m)
                        if (valid_scope(s[i].row + n, size) &&
                            valid_scope(s[i].col + m, size)) {
                                if (pbrd[s[i].row + n][s[i].col + m].content >=
                                    EMPTY &&
                                    pbrd[s[i].row + n][s[i].col + m].content <
                                    MINE &&
                                    !pbrd[s[i].row + n][s[i].col + m].revealed)
                                        {
                                        if (pbrd[s[i].row + n][s[i].col + m].
                                            flag)
                                                continue;
                                        ++j;
                                        s[j] = (struct pos){s[i].row + n,
                                                            s[i].col + m};
                                        stack = true;
                                }
                        }
                }

                if (stack) {
                        i = j;
                        stack = false;
                }
        }

        return revealed;
}

int brd_redraw(struct brd *pbrd[], struct sets *sets, struct pos *brd_pos)
{
        int mine_row, mine_col;
        int revealed = 0;

        pbrd[brd_pos->row][brd_pos->col].content = EMPTY;
        brd_loop(pbrd, brd_pos->row, brd_pos->col, sets->size, &mine_dec_arnd);
        brd_loop(pbrd, brd_pos->row, brd_pos->col, sets->size, &mine_inc_here);

        do {
                mine_row = rand() % (sets->size - 1);
                mine_col = rand() % (sets->size - 1);
        } while (mine_row == brd_pos->row || mine_col == brd_pos->col ||
                 pbrd[mine_row][mine_col].content == MINE);

        pbrd[mine_row][mine_col].content = MINE;
        brd_loop(pbrd, mine_row, mine_col, sets->size, &mine_inc_arnd);

        revealed = empty_reveal(pbrd, sets->size, brd_pos->row,
                                brd_pos->col);

        return revealed;
}

static int empty_cnt(int mines, int size)
{
        return size * size - mines;
}

static void brd_reveal(struct brd *pbrd[], int size)
{
        for (int row = 0; row < size; ++row) {
                for (int col = 0; col < size; ++col) {
                        pbrd[row][col].revealed = true;
                        pbrd[row][col].flag = false;
                }
        }
}

int brd_set(struct brd **pbrd[], struct sets *sets)
{
        int empty;

        brd_alloc(pbrd, sets);
        empty = brd_fill(*pbrd, sets);
        empty = empty_cnt(empty, sets->size);

        return empty;
}

void brd_free(struct brd **pbrd[], int size)
{
        for (int i = 0; i < size; ++i)
                free(pbrd[0][i]);

        free(*pbrd);
}

long int get_time(void)
{
        time_t seconds;
        return time(&seconds);
}

void brd_click(struct brd *pbrd[], struct sets *sets, struct pos *brd_pos,
               SDL_Event e, int *option, int *empty_fields, bool *first_click,
               long int *time_start)
{
        if (*first_click)
                *time_start = get_time();

        switch (e.button.button) {
        case SDL_BUTTON_LEFT:
                if (pbrd[brd_pos->row][brd_pos->col].flag) {
                        break;
                } else {
                        if (pbrd[brd_pos->row][brd_pos->col].content == MINE) {
                                if (*first_click) {
                                        *empty_fields -= brd_redraw(pbrd, sets,
                                                                    brd_pos);
                                        *first_click = false;
                                } else {
                                        pbrd[brd_pos->row]
                                            [brd_pos->col].revealed = true;
                                        *option = END_GAME;
                                        brd_reveal(pbrd, sets->size);
                                }
                        } else {
                                *empty_fields -= empty_reveal(pbrd, sets->size,
                                                 brd_pos->row, brd_pos->col);
                                *first_click = false;
                        }

                        break;
                }
        case SDL_BUTTON_RIGHT:
                if (!pbrd[brd_pos->row][brd_pos->col].flag)
                        pbrd[brd_pos->row][brd_pos->col].flag = true;
                else
                        pbrd[brd_pos->row][brd_pos->col].flag = false;
                break;
        }

        if (!(*empty_fields)) {
                *option = END_GAME;
                brd_reveal(pbrd, sets->size);
        } 
}
