CFLAGS = -O2 -Wall -Wextra -Wconversion
LDFLAGS = -lSDL2 -lSDL2_ttf -lSDL2_image
CC = gcc

main: saper.c
	$(CC) saper.c core.c render.c -o saper $(CFLAGS) $(LDFLAGS)

clean:
	-rm -f saper
