#include <stdio.h>
#include <stdbool.h>
#include <SDL2/SDL.h>

#define EMPTY 0
#define MINE 9

enum {MENU, SETTINGS, BRD_SET, GAME, END_GAME, QUIT};

struct pos {
        int row;
        int col;
};

struct dims {
        int h;
        int w;
};

struct sets {
        int difclty;
        int size;
};

struct brd {
        bool revealed;
        bool flag;
        int content;
};
