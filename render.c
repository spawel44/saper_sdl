#include <stdlib.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include "common.h"

#define TARGET_FPS 60
#define SCREEN_WIDTH  800
#define SCREEN_HEIGHT 800

extern void brd_click(struct brd *pbrd[], struct sets *sets,
                      struct pos *brd_pos, SDL_Event e, int *option,
                      int *empty_fields, bool *first_click,
                      long int *time_start);
extern long int get_time(void);

enum {EDISP1, EDISP2, EDISP3, EDISP4, EDISP5, EDISP6, EDISP7, EDISP8, EDISP9};
const char *rend_err[] = {"Unable to initialize SDL\n",
                          "Unable to initialize SDL_ttf\n",
                          "Unable to initialize SDL_image\n",
                          "Unable to get screen resolution\n",
                          "Unable to create window\n",
                          "Unable to initialize renderer\n",
                          "Font cannot be loaded\n",
                          "Unable to create surface\n",
                          "Unable to set texture\n"
                          };

enum {IMG_FLAG, IMG_MINE, IMG_POLE, IMG_PUSTE};

struct text {
        int size;
        SDL_Color color;
        TTF_Font *font;
        const char *str;
};

struct texture {
        struct dims dims;
        struct pos pos;
        SDL_Texture *texture;
};

static void fps_calc()
{
        static unsigned int last_time = 0;
        static unsigned int frame = 0;
        static unsigned int fps = 0;

        unsigned int current_time = SDL_GetTicks();
        ++frame;

        if (current_time - last_time >= 1000) {
                fps = frame / ((current_time - last_time) / 1000);
                printf("FPS: %d\n", fps);

                frame = 0;
                last_time = current_time;
        }

        unsigned int frame_ticks = SDL_GetTicks() - current_time;
        if (frame_ticks < 1000 / TARGET_FPS) {
                SDL_Delay(1000 / TARGET_FPS - frame_ticks);
        }
}

int sdl_init(void)
{
        if (SDL_Init(SDL_INIT_VIDEO) < 0) {
                fprintf(stdout, "%s", rend_err[EDISP1]);
                exit(EXIT_FAILURE);
        }

        if (TTF_Init() < 0) {
                fprintf(stdout, "%s", rend_err[EDISP2]);
                exit(EXIT_FAILURE);
        }

        if (IMG_INIT_PNG < 0 || IMG_Init(IMG_INIT_PNG) < 0) {
                fprintf(stdout, "%s", rend_err[EDISP3]);
                exit(EXIT_FAILURE);
        }

        return 0;
}

static int res_get(SDL_DisplayMode *screen_res)
{
        if (!SDL_GetCurrentDisplayMode(0, screen_res)) {
                return 0;
        } else {
                fprintf(stdout, "%s", rend_err[EDISP4]);
                exit(EXIT_FAILURE);
        }
}

static void res_set(SDL_DisplayMode *screen_res)
{
        screen_res->w = screen_res->w / 2;
        screen_res->h = screen_res->w;
}

static int window_init(SDL_DisplayMode *screen_res, SDL_Window **window,
                       SDL_Renderer **renderer)
{
        *window = SDL_CreateWindow("Saper", SDL_WINDOWPOS_UNDEFINED,
                                            SDL_WINDOWPOS_UNDEFINED,
                                            screen_res->w, screen_res->h,
                                            SDL_WINDOW_SHOWN);

        if (*window != NULL) {
                *renderer = SDL_CreateRenderer(*window, -1,
                            SDL_RENDERER_ACCELERATED);
        } else {
                fprintf(stdout, "%s", rend_err[EDISP5]);
                exit(EXIT_FAILURE);
        }

        if (*renderer != NULL) {
                return 0;               
        } else {
                fprintf(stdout, "%s", rend_err[EDISP6]);
                exit(EXIT_FAILURE);
        }
}

void disp_init(SDL_DisplayMode *screen_res, SDL_Window **window,
              SDL_Renderer **renderer)
{
        if (!res_get(screen_res)) {
                res_set(screen_res);
        } else {
                screen_res->w = SCREEN_WIDTH;
                screen_res->h = SCREEN_HEIGHT;
        }

        window_init(screen_res, window, renderer);
}

static int font_open(struct text *text)
{
        text->font = TTF_OpenFont("./fonts/LiberationSerif-Regular.ttf",
                                  text->size);

        if (text->font != NULL) {
                return 0;
        } else {
                fprintf(stdout, "%s", rend_err[EDISP7]);
                exit(EXIT_FAILURE);
        }
}

static int font_set(TTF_Font *font, const char *text, SDL_Color color,
                    struct texture *texture, SDL_Renderer *renderer)
{
        SDL_Surface *surface = TTF_RenderText_Blended(font, text, color);
        TTF_CloseFont(font);

        if (surface != NULL) {
                texture->texture = SDL_CreateTextureFromSurface(renderer,
                                                                surface);
                texture->dims.w = surface->w;
                texture->dims.h = surface->h;
                SDL_FreeSurface(surface);

                if (texture != NULL) {
                        return 0;
                } else {
                        fprintf(stdout, "%s", rend_err[EDISP9]);
                        exit(EXIT_FAILURE);
                }
        } else {
                fprintf(stdout, "%s", rend_err[EDISP8]);
                exit(EXIT_FAILURE);
        }
}

static void text_set(struct texture *texture, struct text *text,
                     SDL_Renderer *renderer)
{
        font_open(text);
        font_set(text->font, text->str, text->color, texture, renderer);
}

static void text_draw(SDL_Renderer *renderer, struct texture *texture,
                      int row, int col)
{
        SDL_Rect destination = {col, row, texture->dims.w, texture->dims.h};
        SDL_RenderCopy(renderer, texture->texture, NULL, &destination);
}
static int image_open(const char *path, struct texture *texture,
                      SDL_Renderer *renderer)
{
        SDL_Surface *surface = IMG_Load(path);

        if (surface != NULL) {
                texture->texture = SDL_CreateTextureFromSurface(renderer,
                                                                surface);
                texture->dims.w = surface->w;
                texture->dims.h = surface->h;
                SDL_FreeSurface(surface);

                if (texture != NULL) {
                        return 0;
                } else {
                        fprintf(stdout, "%s", rend_err[EDISP9]);
                        exit(EXIT_FAILURE);
                }
        } else {
                fprintf(stdout, "%s", rend_err[EDISP8]);
                exit(EXIT_FAILURE);
        }
}

void image_draw(SDL_Renderer *renderer, struct texture *texture,
                int row, int col)
{
        SDL_Rect destination = {col, row, texture->dims.w, texture->dims.h};
        SDL_RenderCopy(renderer, texture->texture, NULL, &destination);
}

static void texture_free(struct texture *texture)
{
        SDL_DestroyTexture(texture->texture);
}

int welcome(SDL_DisplayMode *screen_res, SDL_Renderer *renderer)
{
        const char *str1 = "Welcome to Saper";
        const char *str2 = "click to continue";
        struct text text1 = {64, (SDL_Color) {255, 0, 0, 255}, NULL, str1};
        struct text text2 = {48, (SDL_Color) {0, 255, 0, 255}, NULL, str2};
        struct texture texture1;
        struct texture texture2;

        text_set(&texture1, &text1, renderer);
        text_set(&texture2, &text2, renderer);

        texture1.pos = (struct pos) {0, 0};
        texture2.pos.col = (screen_res->w - texture2.dims.w) / 2;
        texture2.pos.row = (screen_res->h - texture2.dims.h) / 2;

        /* -1 because we're exiting on MENU and QUIT, MENU is 0 */
        int retval = -1;
        bool draw_next = false;
        int i = 0, j = 0;

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        for (SDL_Event e; e.type != SDL_QUIT;) {
                while (SDL_PollEvent(&e)) {
                        if (e.type == SDL_MOUSEBUTTONDOWN &&
                            e.button.button == SDL_BUTTON_LEFT) {
                                retval = MENU;
                        } else if (e.type == SDL_QUIT) {
                                retval = QUIT;
                        }
                }

                if (retval >= MENU) {
                        break;
                }

                SDL_RenderClear(renderer);

                text_draw(renderer, &texture1, 0, i);
                if (i + texture1.dims.w >= screen_res->w && !draw_next) {
                        j = -texture1.dims.w;
                        draw_next = true;
                }

                if (j <= 0) {
                        text_draw(renderer, &texture1, 0, j);
                        ++j;
                }

                if (j == 0) {
                        i = j;
                        draw_next = false;
                }
                ++i;

                text_draw(renderer, &texture2, texture2.pos.row,
                          texture2.pos.col);

                SDL_RenderPresent(renderer);
                fps_calc();
        }

        texture_free(&texture1);
        texture_free(&texture2);

        return retval;
}

static int click_on(struct pos *mouse, struct pos *item, struct dims *dims)
{
        if (mouse->row >= item->row && mouse->row <= item->row + dims->h &&
            mouse->col >= item->col && mouse->col <= item->col + dims->w)
                return 1;
        else
                return 0;
}

int menu(SDL_DisplayMode *screen_res, SDL_Renderer *renderer)
{
        const char *str1 = "New game";
        const char *str2 = "Quit";
        struct text text1 = {48, (SDL_Color) {255, 0, 0, 255}, NULL, str1};
        struct text text2 = {48, (SDL_Color) {255, 0, 0, 255}, NULL, str2};
        struct texture texture1;
        struct texture texture2;

        text_set(&texture1, &text1, renderer);
        text_set(&texture2, &text2, renderer);

        texture1.pos.col = (screen_res->w - texture1.dims.w) / 2;
        texture1.pos.row = (screen_res->h - texture1.dims.h) / 3;
        texture2.pos.col = texture1.pos.col;
        texture2.pos.row = texture1.pos.row + texture1.dims.h;

        int option = MENU;
        bool update = true;
        struct pos mouse;

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        for (SDL_Event e; e.type != SDL_QUIT && option == MENU;) {
                while (SDL_PollEvent(&e)) {
                        update = true;
                        if (e.type == SDL_QUIT) {
                                option = QUIT;
                                continue;
                        } else if (e.type == SDL_MOUSEBUTTONDOWN &&
                                   e.button.button == SDL_BUTTON_LEFT) {
                                SDL_GetMouseState(&mouse.col, &mouse.row);
                                if (click_on(&mouse, &texture1.pos,
                                             &texture1.dims)) {
                                               option = SETTINGS;
                                        continue;
                                } else if (click_on(&mouse, &texture2.pos,
                                                    &texture2.dims)) {
                                        option = QUIT;
                                        continue;
                                }
                        }
                }

                if (update) {
                        SDL_RenderClear(renderer);

                        text_draw(renderer, &texture1, texture1.pos.row,
                                  texture1.pos.col);
                        text_draw(renderer, &texture2, texture2.pos.row,
                                  texture2.pos.col);

                        SDL_RenderPresent(renderer);
                        update = false;
                }

                fps_calc();
        }

        texture_free(&texture1);
        texture_free(&texture2);

        return option;
}

void settings(SDL_DisplayMode *screen_res, SDL_Renderer *renderer,
              struct sets *sets)
{
        enum {DIFCLTY, EASY_DIF, MEDIUM_DIF, HARD_DIF, SIZE, SMALL_SIZ,
              MEDIUM_SIZ, LARGE_SIZ, BACK, LEN};

        const char *str_arr[] = {"choose difficulty:", "easy", "medium", "hard",
                                 "choose size:", "small", "medium", "large",
                                 "back"};

        struct text text_arr[LEN];
        for (int i = 0; i < LEN; ++i)
                text_arr[i] = (struct text) {48, (SDL_Color) {255, 0, 0, 255},
                                             NULL, str_arr[i]};

        struct texture texture_arr[LEN];
        for (int i = 0; i < LEN; ++i)
                text_set(&texture_arr[i], &text_arr[i], renderer);

        /* set difficulty menu position */
        texture_arr[DIFCLTY].pos = (struct pos) {(screen_res->w -
                                    texture_arr[DIFCLTY].dims.h) / 4,
                                    (screen_res->w - texture_arr[DIFCLTY].
                                     dims.w) / 2};

        for (int i = EASY_DIF; i <= HARD_DIF; ++i)
                texture_arr[i].pos = (struct pos)
                                     {texture_arr[i - 1].pos.row +
                                      texture_arr[i - 1].dims.h,
                                      (screen_res->w -
                                      texture_arr[i].dims.w) / 2,
                                      };

        /* set size menu position */
        texture_arr[SIZE].pos = (struct pos) {(screen_res->w -
                                texture_arr[SIZE].dims.h) / 4,
                                (screen_res->w -
                                texture_arr[SIZE].dims.w) / 2};

        for (int i = SMALL_SIZ; i <= LARGE_SIZ; ++i)
                texture_arr[i].pos = (struct pos)
                                     {texture_arr[i - 1].pos.row +
                                      texture_arr[i - 1].dims.h,
                                      (screen_res->w -
                                      texture_arr[i].dims.w) / 2};

        bool dif_set = false;
        bool size_set = false;
        bool update = true;
        struct pos mouse;

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        for (SDL_Event e; e.type != SDL_QUIT;) {
                while (SDL_PollEvent(&e)) {
                        update = true;
                        if (e.type == SDL_QUIT) {
                                break;
                        } else if (e.type == SDL_MOUSEBUTTONDOWN &&
                                   e.button.button == SDL_BUTTON_LEFT) {

                                SDL_GetMouseState(&mouse.col, &mouse.row);
                                if (!dif_set) {
                                        if (click_on(&mouse,
                                            &texture_arr[EASY_DIF].pos,
                                            &texture_arr[EASY_DIF].dims)) {
                                                sets->difclty = 0;
                                                dif_set = true;
                                        } else if (click_on(&mouse,
                                               &texture_arr[MEDIUM_DIF].pos,
                                               &texture_arr[MEDIUM_DIF].dims)) {
                                                sets->difclty = 1;
                                                dif_set = true;
                                        } else if (click_on(&mouse,
                                               &texture_arr[HARD_DIF].pos,
                                               &texture_arr[HARD_DIF].dims)) {
                                                sets->difclty = 2;
                                                dif_set = true;
                                        }

                                } else if (!size_set) {
                                        if (click_on(&mouse,
                                               &texture_arr[SMALL_SIZ].pos,
                                               &texture_arr[SMALL_SIZ].dims)) {
                                                sets->size = 0;
                                                size_set = true;
                                                e.type = SDL_QUIT;
                                                break;
                                        } else if (click_on(&mouse,
                                               &texture_arr[MEDIUM_SIZ].pos,
                                               &texture_arr[MEDIUM_SIZ].dims)){
                                                sets->size = 1;
                                                size_set = true;
                                                e.type = SDL_QUIT;
                                                break;
                                        } else if (click_on(&mouse,
                                               &texture_arr[LARGE_SIZ].pos,
                                               &texture_arr[LARGE_SIZ].dims)) {
                                                sets->size = 2;
                                                size_set = true;
                                                e.type = SDL_QUIT;
                                                break;
                                        } else if (click_on(&mouse,
                                               &texture_arr[BACK].pos,
                                               &texture_arr[BACK].dims)) {
                                                e.type = SDL_QUIT;
                                                break;
                                        }
                                }
                        }
                }

                if (update) {
                        SDL_RenderClear(renderer);

                        if (!dif_set) {
                                for (int i = DIFCLTY; i <= HARD_DIF; ++i)
                                        text_draw(renderer, &texture_arr[i],
                                            texture_arr[i].pos.row,
                                            texture_arr[i].pos.col);
                        } else if (!size_set) {
                                for (int i = SIZE; i <= LARGE_SIZ; ++i)
                                        text_draw(renderer, &texture_arr[i],
                                            texture_arr[i].pos.row,
                                            texture_arr[i].pos.col);
                        }

                        SDL_RenderPresent(renderer);
                        update = false;
                }

                fps_calc();
        }

        for (int i = 0; i < LEN; ++i)
                texture_free(&texture_arr[i]);
}

static void draw_field(SDL_Renderer *renderer, int row, int col,
                       struct pos *brd_pos, struct brd *pbrd[],
                       struct texture *text_texture_arr,
                       struct texture *img_texture_arr)
{
        int content = pbrd[brd_pos->row][brd_pos->col].content;
        int flag = pbrd[brd_pos->row][brd_pos->col].flag;

        if (pbrd[brd_pos->row][brd_pos->col].revealed) {
                switch (content) {
                case EMPTY:
                        image_draw(renderer, &img_texture_arr[IMG_PUSTE],
                                   row, col);
                        break;
                case MINE:
                        image_draw(renderer, &img_texture_arr[IMG_PUSTE],
                                   row, col);
                        image_draw(renderer, &img_texture_arr[IMG_MINE],
                                   row, col);
                        break;
                default:
                        /* draw empty field and text */
                        image_draw(renderer, &img_texture_arr[IMG_PUSTE],
                                   row, col);
                        text_draw(renderer, &text_texture_arr[content - 1],
                                  row - (text_texture_arr[content - 1].dims.h -
                                  img_texture_arr[IMG_PUSTE].dims.h) / 2, col -
                                  (text_texture_arr[content - 1].dims.w -
                                  img_texture_arr[IMG_PUSTE].dims.w) / 2);
                        break;
                }
        } else {
                switch (flag) {
                case true:
                        image_draw(renderer, &img_texture_arr[IMG_PUSTE],
                                   row, col);
                        image_draw(renderer, &img_texture_arr[IMG_FLAG],
                                   row, col);
                        break;
                default:
                        image_draw(renderer, &img_texture_arr[IMG_PUSTE],
                                   row, col);
                        image_draw(renderer, &img_texture_arr[IMG_POLE],
                                   row, col);
                        break;
                }
        }
}

int game(SDL_DisplayMode *screen_res, SDL_Renderer *renderer,
         struct brd *pbrd[], struct sets *sets, int *empty_fields,
         bool *first_click, long int *time_end)
{
        int space = screen_res->w / sets->size / 10;
        int size = screen_res->w / sets->size - space;
 
        const char *str[] = {"1", "2", "3", "4", "5", "6", "7", "8"};
        enum {str_arr_len = 8};

        struct text text_arr[str_arr_len];
        for (int i = 0; i < str_arr_len; ++i) {
                text_arr[i] = (struct text) {size, (SDL_Color) {0, 0, 255,
                                             255}, NULL, str[i]};
        }

        struct texture text_texture_arr[str_arr_len];
        for (int i = 0; i < str_arr_len; ++i)
                text_set(&text_texture_arr[i], &text_arr[i], renderer);


        const char *img_arr[] = {"./images/flag.png", "./images/mine.png",
                                 "./images/pole.png", "./images/puste.png"};

        enum {img_arr_len = IMG_PUSTE + 1};

        struct texture img_texture_arr[IMG_PUSTE + 1];
        for (int i = 0; i < img_arr_len; ++i) {
                image_open(img_arr[i], &img_texture_arr[i], renderer);
                img_texture_arr[i].dims = (struct dims) {size, size};
        }
        
        int option = GAME;
        int field_row = space / 2;
        int field_col = space / 2;
        long int time_start = 0;
        struct pos mouse, brd_pos, field;   
        struct dims dims = {size, size};
        bool update = true;

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        for (SDL_Event e; e.type != SDL_QUIT && option == GAME;) {
                while (SDL_PollEvent(&e)) {
                        update = true;
                        if (e.type == SDL_QUIT) {
                                option = QUIT;
                                continue;
                        } else if (e.type == SDL_MOUSEBUTTONDOWN) {
                                SDL_GetMouseState(&mouse.col, &mouse.row);
                                break;
                        }
                }

                if (update) {
                        for (int row = 0; row < sets->size; ++row) {
                                for (int col = 0; col < sets->size; ++col) {
                                        brd_pos = (struct pos) {row, col};
                                        field = (struct pos) {field_row,
                                                              field_col};
                                        if (click_on(&mouse, &field, &dims)) {
                                                brd_click(pbrd, sets,
                                                &brd_pos, e, &option,
                                                empty_fields, first_click,
                                                &time_start);
                                        }

                                        field_row += space + size;
                                }

                                field_row = space / 2;
                                field_col += space + size;
                        }

                        field_row = space / 2;
                        field_col = space / 2;

                        SDL_RenderClear(renderer);
                        for (int row = 0; row < sets->size; ++row) {
                                for (int col = 0; col < sets->size; ++col) {
                                        brd_pos = (struct pos) {row, col};
                                        field = (struct pos) {field_row,
                                                              field_col};
                                        draw_field(renderer, field_row,
                                                   field_col, &brd_pos, pbrd,
                                                   text_texture_arr,
                                                   img_texture_arr);
                                        field_row += space + size;
                                }

                                field_row = space / 2;
                                field_col += space + size;
                        }

                        field_row = space / 2;
                        field_col = space / 2;

                        SDL_RenderPresent(renderer);
                        update = false;
                }

                fps_calc();
        }

        *time_end = get_time() - time_start;

        for (int i = 0; i < str_arr_len; ++i)
                texture_free(&text_texture_arr[i]);

        for (int i = 0; i < img_arr_len; ++i)
                texture_free(&img_texture_arr[i]);

        return option;
}

void long_to_str(long int num, char *arr, int len)
{
        int i = 0;
        char tmp_arr[len];

        do {
                tmp_arr[i] = (char) (num % 10) + '0';
                num /= 10;
                ++i;
        } while (num && i < len - 1);
        
        arr[i] = '\0';
        --i;
        for (int j = 0; i >= 0; --i, ++j)
                arr[j] = tmp_arr[i];
}

int end_dial(SDL_DisplayMode *screen_res, SDL_Renderer *renderer,
             int empty_fields, long int time_end)
{
        const char *str1 = "You have won!";
        const char *str2 = "Oops.. you hit a mine!";
        struct text end_dialog = {64, (SDL_Color) {255, 0, 0, 255}, NULL, NULL};
        struct text game_time = {48, (SDL_Color) {255, 0, 0, 255}, NULL, NULL};

        if (!empty_fields)
                end_dialog.str = str1;
        else
                end_dialog.str = str2;

        /* maybe consider static */
        char arr[100];
        long_to_str(time_end, arr, 100);
        /* looks ugly, but should work ↓ */
        game_time.str = (const char *) &arr;

        struct texture texture1;
        struct texture texture2;

        text_set(&texture1, &end_dialog, renderer);
        text_set(&texture2, &game_time, renderer);

        texture1.pos.col = (screen_res->w - texture1.dims.w) / 2;
        texture1.pos.row = 0;
        texture2.pos.col = (screen_res->w - texture2.dims.w) / 2;
        texture2.pos.row = (screen_res->h - texture2.dims.h) / 2;

        int retval = -1;
        bool update = true;

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        for (SDL_Event e; e.type != SDL_QUIT;) {
                while (SDL_PollEvent(&e)) {
                        update = true;
                        if (e.type == SDL_MOUSEBUTTONDOWN &&
                            e.button.button == SDL_BUTTON_LEFT) {
                                retval = MENU;
                        } else if (e.type == SDL_QUIT) {
                                retval = QUIT;
                        }
                }

                if (retval >= MENU) {
                        break;
                }

                if (update) {
                        SDL_RenderClear(renderer);

                        text_draw(renderer, &texture1, 0, texture1.pos.col);
                        text_draw(renderer, &texture2, texture2.pos.row,
                                  texture2.pos.col);
                        SDL_RenderPresent(renderer);
                        update = false;
                }

                fps_calc();
        }

        texture_free(&texture1);
        texture_free(&texture2);

        return retval;
}
